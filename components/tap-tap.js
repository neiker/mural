import React, { PropTypes } from 'react';

import { TouchableWithoutFeedback } from 'react-native';

export class TapTap extends React.Component {
    constructor(props) {
        super(props);

        this.props = props;
    }

    tap(e) {
      const { handler } = this.props;


      if (this.recentTap) {
          delete this.recentTap;

          handler({x: e.nativeEvent.locationX, y: e.nativeEvent.locationY});
      } else {
          this.recentTap = true;
          setTimeout(() => delete this.recentTap, 300);
      }
    }

    render () {
        return (
            <TouchableWithoutFeedback onPress={(e) => this.tap(e)}>
                {this.props.children}
            </TouchableWithoutFeedback>
        )
    }
}


TapTap.propTypes = {
  handler: PropTypes.func.isRequired,
  children: PropTypes.oneOfType([
    PropTypes.element,
    PropTypes.string,
  ]).isRequired,
};
