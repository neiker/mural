import React, { PropTypes } from 'react';

import {
    StyleSheet,
    Image,
    Text,
    View,
    Animated,
    PanResponder
} from 'react-native';

import { connect } from 'react-redux';

import { toggleOption } from '../state/actions';

var styles = StyleSheet.create({
    animatedView: {
        position: 'absolute'
    }
});

class MuralDraggablePresentation extends React.Component {
    constructor(props) {
        super();
        this.props = props;

        this.scale = new Animated.Value(1);
    }

    componentWillMount() {
      const { toggleScrollBlock, onDrag } = this.props;

      const offset = {x: 0, y: 0};

      this._panResponder = PanResponder.create({
        onStartShouldSetPanResponder: (evt, gestureState) => {
            if (this.recentTap) {
                delete this.recentTap;

                if (this.props.onTapTap) {
                    this.props.onTapTap();
                }

                return false;
            } else {
                this.recentTap = true;
                setTimeout(() => delete this.recentTap, 300);
            }

            return true;
        },
        onPanResponderGrant: (e) => {
          toggleScrollBlock(true);

          offset.x = this.props.position.x;
          offset.y = this.props.position.y;

          Animated.spring(this.scale, { toValue: 1.1, friction: 5 }).start();
        },
        onPanResponderMove: (e, pos)=> {
            const zoomScale =  this.props.zoomScale || 1;

            onDrag((pos.dx / zoomScale) + offset.x, (pos.dy / zoomScale) + offset.y);
        },
        onPanResponderRelease: (e) => {
            toggleScrollBlock(false);

            Animated.spring(this.scale, { toValue: 1, friction: 5 } ).start();
        }
      });
    }

    render() {
        const { position } = this.props;

        const transform = {
            transform: [
                {translateX: position.x},
                {translateY: position.y},
                {scale: this.scale}
            ]
        };

        return (
            <Animated.View
                style={[transform, styles.animatedView]}
                {...this._panResponder.panHandlers}>
                {this.props.children}
            </Animated.View>
        );
    }
}
MuralDraggablePresentation.propTypes = {
  toggleScrollBlock: PropTypes.func.isRequired,

  onDrag: PropTypes.func.isRequired,
  onTapTap: PropTypes.func,

  children: PropTypes.oneOfType([
    PropTypes.element,
    PropTypes.string,
  ]).isRequired,

  zoomScale: PropTypes.number
};

export const MuralDraggable = connect(
  state => ({
      zoomScale: state.options.get('zoomScale')
  }),
  dispatch => ({
    toggleScrollBlock: value => dispatch(toggleOption('scrollBlock', value)),
  })
)(MuralDraggablePresentation);
