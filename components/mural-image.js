import React, { PropTypes } from 'react';

import {
    StyleSheet,
    Image
} from 'react-native';

var styles = StyleSheet.create({
    img: {
        width: 100,
        height: 100
    }
});

export const MuralImage = (props) => {
    const { path } = props;

    return (
        <Image style={styles.img} source={{uri:path}} />
    );
};
MuralImage.propTypes = {
  path: PropTypes.string.isRequired
};
