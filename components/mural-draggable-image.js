import React from 'react';
import { connect } from 'react-redux';

import { MuralImage } from './mural-image';
import { TapTap } from './tap-tap';
import { MuralDraggable } from './mural-draggable';
import { moveImage, deleteImage } from '../state/actions';

const MuralDraggableImagePresentation = ({image, updatePosition, deleteImage}) => (
    <MuralDraggable position={image.get('position')} onDrag={updatePosition} onTapTap={deleteImage}>
        <MuralImage path={image.get('path')}></MuralImage>
    </MuralDraggable>
);


export const MuralDraggableImage = connect(
  state => ({

  }),
  (dispatch, props) => ({
    updatePosition: (x, y) => dispatch(moveImage(props.image.get('id'), x, y)),
    deleteImage: () => dispatch(deleteImage(props.image.get('id'))),
  })
)(MuralDraggableImagePresentation);
