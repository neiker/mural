import React from 'react';

import { connect } from 'react-redux';

import {
  StyleSheet,
  ScrollView,
  Text,
  View,
} from 'react-native';

import { MuralDraggableImage } from './mural-draggable-image';
import { TapTap } from './tap-tap';

import { addImage, toggleOption } from '../state/actions';

var styles = StyleSheet.create({
  container: {
    top: 20,
    flex: 1,
  },
  canvas: {
    flex: 1,
    backgroundColor: '#EEEEEE',
    width: 9000,
    height: 6000
  }
});



class MuralPresentation extends React.Component {
  constructor(props) {
      super(props);

      this.props = props;
  }

  onScroll(e) {
      const { zoomScale } = e.nativeEvent;

      if (this.zoomScale !== zoomScale) {
          this.props.zoomChange(zoomScale);

          this.zoomScale = zoomScale;
      }
  }

  render() {
    const { scrollBlock, images, canvasDoubleTap } = this.props;

    return (
      <View style={styles.container}>
        <ScrollView
          scrollEnabled={!scrollBlock}
          bouncesZoom={true}
          maximumZoomScale={2}
          minimumZoomScale={0.1}
          horizontal={true}
          onScroll={(e) => this.onScroll(e)}
          scrollEventThrottle = {50}
        >
            <TapTap handler={(position) => canvasDoubleTap(position)}>
                <View style={styles.canvas}>
                    {images.map(image => (
                        <MuralDraggableImage image={image} key={image.get('id')}></MuralDraggableImage>
                    ))}
                </View>
            </TapTap>
        </ScrollView>
      </View>
    );
  }
};

export const Mural = connect(
  state => ({
    scrollBlock: state.options.get('scrollBlock'),
    images: state.images
  }),
  dispatch => ({
     canvasDoubleTap: (position) => dispatch(addImage(position)),
     zoomChange: zoomScale => dispatch(toggleOption('zoomScale', zoomScale))
  })
)(MuralPresentation);
