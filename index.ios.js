import React, { Component } from 'react';
import { AppRegistry } from 'react-native';
import { createStore } from 'redux';
import { Provider } from 'react-redux';

import { Mural } from './components/mural.js';
import reducers from './state/reducers';

const container = () => (
    <Provider store={createStore(reducers)}>
      <Mural />
    </Provider>
);

AppRegistry.registerComponent('mural', () => container);
