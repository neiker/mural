/*
 * action types
 */

// IMAGES
export const ADD_IMAGE = 'ADD_IMAGE';
export const MOVE_IMAGE = 'MOVE_IMAGE';
export const DELETE_IMAGE = 'DELETE_IMAGE';

// OPTIONS
export const TOGGLE_OPTION = 'TOGGLE_OPTION';


/*
 * action creators
 */

let id = 0;
const images = [
    'http://i.imgur.com/CUikePc.png?1',
    'http://i.imgur.com/iubkxLL.jpg',
    'http://i.imgur.com/mLSmAuL.jpg',
    'http://i.imgur.com/sMdqQBW.jpg',
    'http://i.imgur.com/7SBTtvM.jpg',
    'http://i.imgur.com/vXA6HzQ.jpg',
    'http://i.imgur.com/UjNJr74.jpg',
    'http://i.imgur.com/8okLKAU.jpg',
    'http://i.imgur.com/jD5Y8k5.jpg',
    'http://i.imgur.com/txGj1Nx.jpg',
    'http://i.imgur.com/9dIubNy.jpg'
];

export function addImage(position) {
  return {
    type: ADD_IMAGE,
    id: id++,
    path: images[Math.floor(Math.random()*images.length)],
    position
  };
}

export function moveImage(id, x, y) {
  return {
    type: MOVE_IMAGE,
    id,
    position: {x, y}
  };
}

export function deleteImage(id) {
  return {
    type: DELETE_IMAGE,
    id
  };
}

export function toggleOption(option, value) {
  return {
    type: TOGGLE_OPTION,
    option,
    value
  };
}
