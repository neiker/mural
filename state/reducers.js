import { combineReducers } from 'redux';
import { List, Map } from 'immutable';

import { ADD_IMAGE, MOVE_IMAGE, DELETE_IMAGE } from './actions';
import { TOGGLE_OPTION } from './actions';

function image(state, action) {
  switch (action.type) {
    case ADD_IMAGE:
      return new Map({
        id: action.id,
        path: action.path,
        position: action.position || {x: 0, y: 0}
      });
    case MOVE_IMAGE:
        if (state.get('id') !== action.id) {
            return state;
        }

        return state.set('position', action.position);
    default:
      return state;
  }
}

function images(state = new List(), action) {
  switch (action.type) {
    case ADD_IMAGE:
      return state.push(
        image(undefined, action)
      );
    case MOVE_IMAGE:
      return state.map(t =>
          image(t, action)
      );
    case DELETE_IMAGE:
        return state.filter(t =>
            t.get('id') !== action.id
        );
    default:
      return state;
  }
}

function options(state = new Map(), action) {
    switch (action.type) {
      case TOGGLE_OPTION:
        if (!action.option) {
            throw new Error('You must specify an option');
        }

        if (action.value !== undefined) {
            return state.set(action.option, action.value);
        } else {
            return state.update(action.option, value => !value);
        }
      default:
        return state;
    }
}

const reducers = combineReducers({
  options,
  images,
});

export default reducers;
